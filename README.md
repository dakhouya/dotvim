How to setup dotfile and configure plugins

```
1 - git clone git@bitbucket.org:dakhouya/dotvim.git ~/.vim
2 - ln -s ~/.vim/vimrc ~.vimrc
3 - cd ~./vim
4 - git submodule init
5 - git submodule update
5 - vim +PluginInstall +qall
```
